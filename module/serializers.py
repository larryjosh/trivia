__author__ = 'stikks'

# import third party modules
from rest_framework import serializers, status, response, validators

# import application modules
from .models import Attempt, Question, Choices, Trivia, Rules


class QuestionSerializer(serializers.ModelSerializer):
    """ custom serializer for serializing and deserializing a user instance into json representation
    """
    class Meta:
        model = Question
        fields = ('problem', "position", 'date_created', 'date_modified', 'incorrect_response')

    def create(self, validated_data):
        """
        Create and return a new `Promo` instance, given the validated data.
        """
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates an existing 'Promo' instance, given the validated data.
        """
        instance.problem = validated_data.get('problem', instance.problem)
        instance.position = validated_data.get('position', instance.position)
        instance.incorrect_response = validated_data.get("incorrect_response", instance.incorrect_response)
        instance.save()
        return instance


class ChoiceSerializer(serializers.Serializer):
    """ custom serializer for serializing and deserializing a user instance into json representation
    """
    code = serializers.CharField(required=True)
    value = serializers.CharField(max_length=200, required=True)
    question_id = serializers.IntegerField(required=True)

    def create(self, validated_data):
        """
        Create and return a new `Choice` instance, given the validated data.
        """
        return Choices.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates an existing 'Choice' instance, given the validated data.
        """
        instance.code = validated_data.get('code', instance.code)
        instance.value = validated_data.get('value', instance.value)
        instance.save()
        return instance


class TriviaSerializer(serializers.ModelSerializer):
    """ custom serializer for serializing and deserializing a user instance into json representation
    """
    class Meta:
        model = Trivia
        fields = ('short_code', 'name', 'invalid', 'incorrect_keyword', 'date_created', 'date_modified',
                  'limit_response', 'keyword', 'subscribe_message', "service_name", "mt_short_code",
                  "draw_winners", "draw_time")
        validators = [
            validators.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('short_code', 'name', 'invalid', 'incorrect_keyword', "limit_response")
            )
        ]

    def create(self, validated_data):
        """
        Create and return a new `Choice` instance, given the validated data.
        """
        return Trivia.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates an existing 'Choice' instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.short_code = validated_data.get('short_code', instance.short_code)
        instance.invalid = validated_data.get('invalid', instance.invalid)
        instance.incorrect_keyword = validated_data.get('incorrect_keyword', instance.incorrect_keyword)
        instance.limit_response = validated_data.get('limit_response', instance.limit_response)
        instance.subscribe_message = validated_data.get('subscribe_message', instance.subscribe_message)
        instance.keyword = validated_data.get('keyword', instance.keyword)
        instance.service_name = validated_data.get('service_name', instance.service_name)
        instance.mt_short_code = validated_data.get('mt_short_code', instance.service_name)
        instance.draw_time = validated_data.get('draw_time', instance.draw_time)
        instance.draw_winners = validated_data.get('draw_winners', instance.draw_winners)
        instance.save()
        return instance


class AttemptSerializer(serializers.ModelSerializer):
    """ custom serializer for serializing and deserializing a promo code instance into json representation
    """
    class Meta:
        model = Attempt
        fields = ('current_question', 'msisdn', 'next_question', 'is_completed', 'is_expired', 'date_created', 'date_modified')

    def create(self, validated_data):
        """return a new `Promo Code` instance, given the validated data."""
        return Attempt.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates an existing 'Push Message' instance, given the validated data.
        """
        instance.current_question = validated_data.get('current_question', instance.current_question)
        instance.next_question = validated_data.get('next_question', instance.next_question)
        instance.is_completed = validated_data.get('is_completed', instance.is_completed)
        instance.is_expired = validated_data.get('is_expired', instance.is_expired)
        instance.save()
        return instance


class OptInSerializer(serializers.Serializer):
    message = serializers.CharField(required=True)
    msisdn = serializers.CharField(required=True)
    trivia = serializers.CharField(required=True)


class NewAttemptSerializer(serializers.Serializer):
    message = serializers.CharField(required=True)
    msisdn = serializers.CharField(required=True)
    trivia = serializers.CharField(required=True)


class RuleSerializer(serializers.Serializer):
    """ custom serializer for serializing and deserializing a user instance into json representation
    """
    number = serializers.CharField(required=True)
    response = serializers.CharField(max_length=200, required=True)
    trivia_id = serializers.IntegerField(required=True)

    def create(self, validated_data):
        """
        Create and return a new `Choice` instance, given the validated data.
        """
        return Rules.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates an existing 'Choice' instance, given the validated data.
        """
        instance.number = validated_data.get('number', instance.number)
        instance.response = validated_data.get('response', instance.response)
        instance.save()
        return instance
