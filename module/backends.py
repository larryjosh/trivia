__author__ = 'stikks'
from django.conf import settings
from django.contrib.auth.hashers import check_password
from .models import UserModel


class EmailAuthBackend(object):
    """
    A custom authentication backend. Allows users to log in using their email address.
    """

    def authenticate(self, email=None, password=None):
        """
        Authentication method
        """
        try:
            user = UserModel.objects.get(email=email)
            if user and user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            user = UserModel.objects.get(pk=user_id)
            if user:
                return user
            return None
        except UserModel.DoesNotExist:
            return None