from datetime import date

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser
from django.conf import settings
from django.utils.text import slugify


# Create your models here.
class AbstractClass(models.Model):
    """
    Abstract base model class
    """
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ('date_created',)


class UserModel(AbstractBaseUser):
    email = models.EmailField('email address', unique=True, db_index=True)
    username = models.CharField("username", unique=True, max_length=255)

    USERNAME_FIELD = 'email'

    def __unicode__(self):
        return self.email


class Trivia(AbstractClass):
    name = models.CharField(max_length=255)
    short_code = models.IntegerField(blank=False, null=False)
    mt_short_code = models.IntegerField(null=True, blank=True)
    keyword = models.CharField(max_length=255)
    service_name = models.CharField(max_length=255)
    invalid = models.TextField(blank=False, null=False)
    incorrect_keyword = models.TextField(blank=False, null=False)
    limit_response = models.TextField(blank=False, null=False)
    subscribe_message = models.TextField(blank=False, null=False)
    draw_time = models.TimeField(blank=True, null=True)
    draw_winners = models.IntegerField(blank=True, null=True)


class Question(AbstractClass):
    """ Questions model class """
    problem = models.TextField(blank=False)
    position = models.CharField(max_length=10)
    incorrect_response = models.TextField()
    trivia = models.ForeignKey(Trivia)

    def is_final(self):
        """ returns True if question is the last number """
        question_list = Question.objects.filter(trivia=self.trivia.pk).order_by('-position')
        return question_list[0].pk == self.pk


class Choices(AbstractClass):
    """Choice options for questions """
    question = models.ForeignKey(Question, related_name="choices")
    code = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    is_valid = models.BooleanField(default=False)


class Player(AbstractClass):
    msisdn = models.CharField(unique=True, null=False, max_length=20)
    has_access = models.BooleanField(default=False)
    trivia = models.ForeignKey(Trivia)

    @staticmethod
    def check_or_create_player(msisdn, trivia_id):
        """ Checks if a user account exists and creates an account if it doesn't find it
        :rtype : User object
        """
        player = Player.objects.filter(msisdn=msisdn, trivia=trivia_id).first()

        if not player:
            trivia = Trivia.objects.get(pk=trivia_id)
            player = Player.objects.create(msisdn=msisdn, trivia=trivia)
        return player

    def has_played(self):
        """ returns true if this msisdn has completed the questions limit this week """
        attempts = [c.date_created.date().isocalendar()[1] for c in Attempt.objects.filter(player=self.pk,
                                                                                           is_completed=True).all()]

        for attempt in attempts:
            if attempt == date.today().isocalendar()[1]:
                return True

        return False


class Attempt(AbstractClass):
    msisdn = models.CharField(max_length=20)
    current_question = models.IntegerField(default=1)
    next_question = models.IntegerField(default=2)
    is_completed = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)  # gets set to True after each draw
    player = models.ForeignKey(Player, related_name="attempts")
    score = models.IntegerField(default=0)
    trivia = models.ForeignKey(Trivia, related_name="attempts")
    won = models.BooleanField(default=False)

    @classmethod
    def check_or_create(cls, player_id):
        """ returns an incomplete attempt or creates a new one """
        attempt = Attempt.objects.filter(player=player_id, is_completed=False, is_expired=False).first()

        if not attempt:
            player = Player.objects.get(pk=player_id)
            attempt = cls.objects.create(player=player, msisdn=player.msisdn, trivia=player.trivia)

        return attempt


class AttemptResponse(AbstractClass):
    attempt = models.ForeignKey(Attempt, related_name="attempt_responses")
    question = models.ForeignKey(Question, related_name="responses")
    is_correct = models.BooleanField(default=False)
    trivia = models.ForeignKey(Trivia, related_name='responses')


class Draws(AbstractClass):
    """ Daily Draw Winners model class
    """
    name = models.CharField(blank=False, max_length=50)
    attempts = models.ManyToManyField(Attempt)
    trivia = models.ForeignKey(Trivia, related_name="winners")


class Rules(AbstractClass):
    id = models.AutoField(primary_key=True)
    trivia = models.ForeignKey(Trivia, related_name='rules')
    response = models.TextField(blank=True)
    number = models.IntegerField(blank=True)