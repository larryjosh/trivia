from celery.schedules import crontab
from celery.task import periodic_task

__author__ = 'stikks'
from unipath import Path
import os, json
from random import choice
from importlib import import_module
from itertools import groupby
from openpyxl import Workbook
from datetime import date, datetime
import requests, json

from django.db.models import Q

from fanta import settings
from rest_framework.response import Response
from rest_framework import status
from djcelery import celery
from django_redis import get_redis_connection
from fanta import celery_app, mongo_client

from .models import Question, AttemptResponse, Attempt, Choices, Trivia, UserModel, Player, Draws


def setup():
    """
    setup application
    :return:
    """
    admin_account = UserModel.objects.create(username="admin", email="admin@sponge.com")
    admin_account.set_password("admin")
    admin_account.save()

    return True


def compute_response(solution, question_id, attempt_id):
    """
    compute trivia response
    :param solution:
    :param question_id:
    :param attempt_id:
    :return:
    """

    question = Question.objects.get(pk=question_id)

    if not question:
        raise Exception("Question not found")

    trivia = Trivia.objects.get(pk=question.trivia_id)

    if not trivia:
        raise Exception("Trivia not found")

    attempt = Attempt.objects.get(pk=attempt_id)

    if not attempt:
        raise Exception("Attempt not found")

    # check if attempt has been exhausted
    if attempt.is_completed:
        return trivia.limit_response

    # check if user has attempted that question within this current valid period
    attempt_response = AttemptResponse.objects.filter(question_id=question.id, trivia_id=trivia.id,
                                                      attempt_id=attempt.pk).first()

    if not attempt_response:
        attempt_response = AttemptResponse.objects.create(attempt=attempt, question=question, trivia_id=trivia.pk)

    # check if the answer was correct
    correct_choice = Choices.objects.filter(Q(code__iexact=solution) | Q(value__iexact=solution), question=question.pk,
                                            is_valid=True).first()

    # mark response to question as correct
    if correct_choice:
        attempt_response.is_correct = True
        attempt_response.save()

        # increment attempt score to keep track
        attempt.score += 1
        attempt.save()

    # check if the question is the final question
    if question.is_final():
        attempt.is_completed = True
        attempt.next_question = None
        attempt.current_question = question.position
        attempt.save()

        if correct_choice:
            return "Correct choice. " + trivia.limit_response

        return trivia.limit_response

    # check if there's a next question
    next_question = Question.objects.filter(position=attempt.next_question, trivia_id=trivia.id).first()

    if not next_question:
        attempt.is_completed = True
        attempt.next_question = None
        attempt.current_question = question.position
        attempt.save()

        return trivia.limit_response

    # imprint next question on attempt
    attempt.current_question = next_question.pk

    # check if next question exists
    next_check = Question.objects.filter(pk=next_question.pk + 1).first()

    if next_check:
        attempt.next_question = next_question.pk + 1
    else:
        attempt.next_question = None
    attempt.save()

    if question.position == 5 and sum([c.is_correct for c in AttemptResponse.objects.filter(trivia_id=trivia.id,
                                                                                            attempt=attempt.pk,
                                                                                            position__in=[1, 2, 3,
                                                                                                          4])]) == 4:
        return next_question.problem

    if question.position == 4 and sum([c.is_correct for c in AttemptResponse.objects.filter(trivia_id=trivia.id,
                                                                                            attempt=attempt.pk,
                                                                                            position__in=[1, 2,
                                                                                                          3])]) == 3:
        return next_question.problem

    # return next question's problem if response was correct
    if correct_choice:
        return next_question.problem
    else:
        return question.incorrect_response


def create_question(trivia_id, **kwargs):
    """
    return new question
    :param trivia_id:
    :param kwargs:
    :return:
    """
    position = kwargs.get('position')
    problem = kwargs.get('problem')
    question = Question.objects.filter(position=position, problem=problem, trivia=trivia_id).first()

    if question:
        return question, False

    trivia = Trivia.objects.get(pk=trivia_id)
    question = Question.objects.create(trivia=trivia, position=position, problem=problem,
                                       incorrect_response=kwargs.get("incorrect_response"))

    return question, True


def opt_in(trivia_id, msisdn, short_code, service_id, network):
    """
    opt in to trivia
    :param trivia_id:
    :param msisdn:
    :param short_code:
    :param service_id:
    :param network:
    :return:
    """
    trivia = Trivia.objects.get(pk=trivia_id)

    if not trivia:
        return Response({"error": "Trivia not found"}, status=status.HTTP_400_BAD_REQUEST)

    player = Player.check_or_create_player(msisdn=msisdn, trivia_id=trivia.id)

    if player.has_played():
        code = trivia.mt_short_code or short_code
        return send_sms_response(msisdn, code, trivia.invalid, service_id, network)
        # return Response({"error": settings.INVALID_CODE}, status=status.HTTP_400_BAD_REQUEST)

    attempt = Attempt.objects.filter(player=player.pk, is_completed=False, is_expired=False,
                                     trivia_id=trivia.pk).first()

    if attempt:
        question = attempt.current_question
    else:
        question = Question.objects.get(position=1, trivia=trivia.id)

    code = trivia.mt_short_code or short_code

    return send_sms_response(msisdn, code, question.problem, service_id, network)


def attempt_trivia(trivia_id, msisdn, short_code, message, service_id, network):
    """
    attempt trivia
    :param trivia_id:
    :param msisdn:
    :param short_code:
    :param message:
    :param service_id:
    :param network:
    :return:
    """
    trivia = Trivia.objects.get(pk=trivia_id)

    if not trivia:
        return Response({"error": "Trivia not found"}, status=status.HTTP_400_BAD_REQUEST)

    player = Player.objects.filter(msisdn=msisdn, trivia_id=trivia.id).first()

    if not player:
        code = trivia.mt_short_code or short_code
        return send_sms_response(msisdn, code, trivia.incorrect_keyword, service_id, network)
        # return Response({"error": settings.INCORRECT_KEYWORD_MESSAGE}, status=status.HTTP_400_BAD_REQUEST)

    if player.has_played():
        code = trivia.mt_short_code or short_code
        return send_sms_response(msisdn, code, trivia.limit_response, service_id, network)
        # return Response({"error": settings.LIMIT_RESPONSE}, status=status.HTTP_400_BAD_REQUEST)

    attempt = Attempt.check_or_create(player.pk)

    if attempt.is_completed:
        code = trivia.mt_short_code or short_code
        return send_sms_response(msisdn, code, trivia.limit_response, service_id, network)
        # return Response({"error": settings.LIMIT_RESPONSE}, status=status.HTTP_400_BAD_REQUEST)

    question = Question.objects.filter(position=attempt.current_question).first()

    if not question:
        # return send_sms_response(msisdn, short_code, trivia.incorrect_keyword)
        return Response({"error": "Question not found"}, status=status.HTTP_404_NOT_FOUND)

    response = compute_response(message, question_id=question.pk, attempt_id=attempt.pk)

    code = trivia.mt_short_code or short_code
    return send_sms_response(msisdn, code, response, service_id, network)

    # return Response({"response": response}, status=status.HTTP_200_OK)


def send_sms_response(msisdn, short_code, message, service_id, network):
    """
    returns sms response
    :param msisdn:
    :param short_code:
    :param message:
    :param service_id:
    :param network:
    :return:
    """

    url = "%s?username=%s&password=%s&to=%s&text=%s&from=%s&smsc=ETISALAT_BC&dlr_mask=31" % (settings.SEND_SMS_API,
                                                                                             settings.SEVAS_USERNAME,
                                                                                             settings.SEVAS_PASSWORD,
                                                                                             msisdn, message,
                                                                                             short_code)

    now = datetime.now()
    response = requests.get(url)

    if response.status_code == 200:
        message = '[DATETIME: %s][STATUS: SMS SENT][SMSC: %s][FROM: %s][TO: %s][MSG: %s]' % (now, network, short_code,
                                                                                             msisdn, message)
        record_activity_log(short_code, msisdn, message, network, 'SMS SENT', now)
        return response.content

    message = '[DATETIME: %s][STATUS: %s][SMSC: %s][FROM: %s][TO: %s][MSG: %s]' % (now, response.content, network,
                                                                                   short_code, msisdn, message)
    record_activity_log(short_code, msisdn, message, network, response.content, now)

    return message


def record_activity(message, file_path="messages.log"):
    with open(file_path, "a") as f:
        f.write('%s\n' % message)


@celery_app.task
def record_activity_log(short_code, msisdn, message, network, response_status, timestamp):
    """
    records activity log
    :param short_code:
    :param msisdn:
    :param message:
    :param network:
    :param response_status:
    :param timestamp
    :return:
    """
    try:
        db = mongo_client[settings.MONGOD_DATABASE]

        activities = db.activities
        record = {
            'short_code': short_code,
            'msisdn': msisdn,
            'network': network,
            'status': response_status,
            'message': message,
            'timestamp': timestamp
        }
        activities.insert_one(record)
        return True
    except:
        # log.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
        return False


def persist_data(msisdn, data):
    """
    cache subscription data
    :param msisdn:
    :param data:
    :return:
    """
    conn = get_redis_connection()
    conn.set(msisdn, data)
    return True


@celery_app.task(queue='trivia')
def process_request(msisdn, short_code, message, network, service_id):
    """
    process trivia request
    :param msisdn:
    :param short_code:
    :param message:
    :param network:
    :param service_id:
    :return:
    """
    trivia = Trivia.objects.filter(short_code__iexact=short_code).first()

    if not trivia:
        return send_sms_response(msisdn, short_code, settings.INCORRECT_KEYWORD_MESSAGE, service_id, network)

    # check subscription
    check = check_subscription_status(msisdn, short_code, trivia.service_name)

    if not check:
        code = trivia.mt_short_code or short_code
        return send_sms_response(msisdn, code, trivia.subscribe_message, service_id, network)

    messages = message.split(' ')

    if len(messages) >= 2:
        return attempt_trivia(trivia.id, msisdn, short_code, ' '.join(messages[1:]), service_id, network)

    return opt_in(trivia.pk, msisdn, short_code, service_id, network)


def check_subscription_status(msisdn, short_code, service_name):
    """
    return subscription status
    :param msisdn:
    :param short_code:
    :param service_name:
    :return:
    """
    conn = get_redis_connection()
    subscription_data = conn.get(msisdn)

    if subscription_data:
        data = json.loads(subscription_data)
        if datetime.strptime(data.get("subscriptionExpiryDate"), '%Y-%m-%d').date() > date.today():
            return True

    # url = "http://sponge.atp-sevas.com:8585/sevas/api_sevas_subscription_search?msisdn=%s&formatType=json" % msisdn

    url = "http://sponge.atp-sevas.com:8585/sevas/subscription_search_api?msisdn=%s&formatType=json" % msisdn

    try:
        resp = requests.get(url)

        if resp.status_code == 200:
            response_data = json.loads(resp.content)

            if type(response_data) != list and response_data.has_key("actionResponseCode") and response_data[
                "actionResponseCode"] == '2':
                return False

            data = filter(lambda j: j["subscriptionServiceShortCode"] == short_code and
                                    j['subscriptionServiceName'].lower() == service_name.lower(), response_data)

            if len(data) == 0:
                return False

            data.sort(key=lambda i: i['subscriptionExpiryDate'], reverse=True)

            latest_sub = data[0]

            if latest_sub.get("subscriptionStatus") == 'active' and datetime.strptime(
                    latest_sub.get('subscriptionExpiryDate'), '%Y-%m-%d').date() > date.today():
                conn.set(msisdn, json.dumps(latest_sub))
                return True

            return False

        return Response({"error": "Unable to confirm subscription"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        return False


def get_class(module_name, class_name):
    """
    returns class
    :param module_name:
    :param class_name:
    :return:
    """
    try:
        module = import_module(module_name)

        if hasattr(module, class_name):
            return getattr(module, class_name)

    except Exception as e:
        raise e


def build_spreadsheet(period, data):
    if not isinstance(period, str) and not isinstance(data, (list, tuple)):
        raise Exception('Invalid input parameters - %s, %s' % (period, data))

    groups = {}

    if period == "daily":
        date_groupings = groupby(data, key=lambda z: z.date_created.date())

        for key, obj in date_groupings:
            groups[key] = list(obj)

    elif period == "weekly":
        week_groupings = groupby(data, key=lambda z: z.date_created.date().isocalendar()[1])

        for key, obj in week_groupings:
            groups[key] = list(obj)

    elif period == "monthly":
        month_groupings = groupby(data, key=lambda z: z.date_created.date().month)

        for key, obj in month_groupings:
            groups[key] = list(obj)

    else:
        raise Exception('Invalid filtering parameter - %s' % period)

    datetime.now().strftime('%Y-%m-%d %H:%M')
    wb = Workbook()
    worksheet = wb.active

    _row = 1
    worksheet.cell(row=_row, column=1, value="PERIOD")
    worksheet.cell(row=_row, column=2, value="COUNT")
    worksheet.cell(row=_row, column=3, value="MSISDN")

    for key, value in groups.items():
        _row += 1
        worksheet.cell(row=_row, column=1, value=key)
        worksheet.cell(row=_row, column=2, value=len(value))
        worksheet.cell(row=_row, column=3, value=','.join(['%s-%s' % (c.msisdn, c.score) for c in value]))

    return wb


def build_draws_sheet(period, data):
    if not isinstance(period, str) and not isinstance(data, (list, tuple)):
        raise Exception('Invalid input parameters - %s, %s' % (period, data))

    groups = {}

    if period == "daily":
        date_groupings = groupby(data, key=lambda z: z.date_created.date())

        for key, obj in date_groupings:
            groups[key] = list(obj)

    elif period == "weekly":
        week_groupings = groupby(data, key=lambda z: z.date_created.date().week)

        for key, obj in week_groupings:
            groups[key] = list(obj)

    elif period == "monthly":
        month_groupings = groupby(data, key=lambda z: z.date_created.date().month)

        for key, obj in month_groupings:
            groups[key] = list(obj)

    else:
        raise Exception('Invalid filtering parameter - %s' % period)

    wb = Workbook()
    worksheet = wb.active

    _row = 1
    worksheet.cell(row=_row, column=1, value="PERIOD")
    worksheet.cell(row=_row, column=2, value="COUNT")
    worksheet.cell(row=_row, column=3, value="MSISDN")

    for key, value in groups.items():
        _row += 1
        worksheet.cell(row=_row, column=1, value=key)
        worksheet.cell(row=_row, column=2, value=len(value))
        worksheet.cell(row=_row, column=3, value=','.join(['%s-%s' % (c.msisdn, c.score) for c in value]))

    return wb


def compute_draw(data, **kwargs):
    """ rotate the list double the length of the list and pick a random number from the list """

    _data = list(set(data))

    if len(data) == 0 or not isinstance(_data, list):
        raise Exception("data can not be an empty list or an object of another data type")

    for x in range(1, len(_data) * len(_data)):
        y = _data.pop()
        data.insert(0, y)

    selection = choice(_data)

    return selection


def build_draw(trivia_id):
    """
     Pre-scheduled for active trivias
    """

    trivia = Trivia.objects.get(pk=trivia_id)
    attempts = Attempt.objects.filter(date_modified__contains=date.today(), trivia_id=trivia.pk,
                                      is_completed=True).all()
    past_winners = Attempt.objects.filter(won__exact=True, trivia_id=trivia.pk).all()
    choices = sorted([c for c in attempts if c not in past_winners], key=lambda x: x.score)
    choice_ids = [i.id for i in choices]
    winners = []

    draw_name = "%s-%s" % (trivia.name, date.today().strftime('%Y-%m-%d'))
    draw = Draws.objects.create(name=draw_name, trivia_id=trivia.pk)

    for rule in trivia.rules.all():
        for u in range(rule.number):

            if len(choice_ids) == 0:
                break

            winner = choice_ids.pop()
            winners.append(winner)

            attempt = Attempt.objects.get(pk=winner)
            attempt.won = True
            attempt.save()
            draw.attempts.add(attempt)

            send_sms_response(attempt.msisdn, trivia.mt_short_code, rule.response, '', network='etisalat_bc')

        return True
