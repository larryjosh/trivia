from datetime import datetime, date, time, timedelta
from itertools import groupby, chain
import sys
import json

from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect, Http404, render_to_response
from django.contrib.auth import login as django_login, authenticate, logout as django_logout, decorators
from django.db.models import Q
from rest_framework import mixins, generics, status, viewsets, permissions
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view
from openpyxl.writer.excel import save_virtual_workbook

from .models import Question, Attempt, UserModel, AttemptResponse, Draws, Choices, Player, Trivia, Rules
from .serializers import OptInSerializer, NewAttemptSerializer, QuestionSerializer, ChoiceSerializer, \
    TriviaSerializer, RuleSerializer
from fanta import settings
from .service import opt_in, get_class, build_spreadsheet, process_request, build_draws_sheet
import forms


# Create your views here.
@api_view(["GET"])
def api_root(request, format=None):
    return Response({
        'attempts': reverse('attempts', request=request, format=format),
        'new': reverse('new', request=request, format=format),
    })


class OptInView(generics.CreateAPIView):
    """
    """
    queryset = Attempt.objects.all()
    serializer_class = OptInSerializer

    def post(self, request, *args, **kwargs):
        """ Opt in to the Fanta promotional trivia
        """
        message = request.data.get("message")
        short_code = request.data.get("short_code")
        msisdn = request.data.get("msisdn")

        if not short_code:
            return Response({"error": "Missing parameter - short_code"}, status=status.HTTP_400_BAD_REQUEST)

        if not msisdn:
            return Response({"error": "Missing parameter - msisdn"}, status=status.HTTP_400_BAD_REQUEST)

        if not message:
            return Response({"error": "Missing parameter - message"}, status=status.HTTP_400_BAD_REQUEST)

        return opt_in(message, msisdn, short_code)
        # resp = client.submit_job('opt-in', args=request.data.values(), background=True, unique=True)
        # resp = opt_in.delay(**request.data)
        # return Response(resp.task_id, status=status.HTTP_200_OK)


class AttemptView(generics.RetrieveAPIView):
    """
    """
    queryset = Attempt.objects.all()
    serializer_class = NewAttemptSerializer

    def get(self, request, *args, **kwargs):
        """ records a new promo attempt
        """
        message = request.query_params.get("message")
        short_code = request.query_params.get("short_code")
        msisdn = request.query_params.get("msisdn")
        network = request.query_params.get('network')

        if not short_code:
            return Response({"error": "Missing parameter - short_code"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            int(short_code)

        except BaseException:
            return Response({"error": "Invalid parameter - short_code"}, status=status.HTTP_400_BAD_REQUEST)

        if not msisdn:
            return Response({"error": "Missing parameter - msisdn"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            int(msisdn)

        except Exception, e:
            return Response({"error": "Invalid parameter - msisdn"}, status=status.HTTP_400_BAD_REQUEST)

        if not message:
            return Response({"error": "Missing parameter - message"}, status=status.HTTP_400_BAD_REQUEST)

        if not network:
            network = 'etisalat_bc'
            # return Response({"error": "Missing parameter - network"}, status=status.HTTP_400_BAD_REQUEST)

        service_ids = settings.SERVICE_IDS

        if network.lower() in service_ids.keys():
            service_id = service_ids[network.lower()]

        else:
            return Response({"error": "Service ID not found for %s" % network}, status=status.HTTP_400_BAD_REQUEST)

        process_request.delay(msisdn, short_code, message, network, service_id)

        return Response(status=status.HTTP_202_ACCEPTED)


def login(request):
    """
    Log in view
    """
    form = forms.AuthenticationForm()
    errors = None
    if request.method == 'POST':
        form = forms.AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(email=request.POST['email'], password=request.POST['password'])
            if user:
                django_login(request, user)
                return redirect('/')
            else:
                errors = 'Invalid username/password.'
    return render_to_response('app/forms/login.html', {
        'form': form, 'errors': errors
    }, context_instance=RequestContext(request))


def logout(request):
    """
    Log out view
    """
    django_logout(request)
    return redirect('/')


def context_processor(request):
    return {
        "attempts": Attempt.objects,
        "questions": Question.objects,
        "users": UserModel.objects,
        "players": Player.objects,
        "winners": Draws.objects,
        'length': len,
        "limited_questions": Question.objects.all()[:5],
        "today": date.today(),
        "responses": AttemptResponse.objects,
        "year": date.today().year,
        "trivias": Trivia.objects
    }


@decorators.login_required
def index(request):
    """ index view
    """

    players = Player.objects.all()
    responses = AttemptResponse.objects.all()

    latest_users = players.order_by('date_created')[:8]

    failed_attempts = responses.filter(is_correct=False)
    difficulty_rate = groupby(failed_attempts.all(), key=lambda s: s.attempt.current_question)

    success_attempts = responses.filter(is_correct=True)
    success_rate = groupby(success_attempts.all(), key=lambda q: q.attempt.current_question)
    percent_success = percent_failure = completion_rate = daily_average = daily_response = 0
    toughest = None

    if responses.count() > 0:
        percent_success = round(success_attempts.count() / float(responses.count()) * 100, 2)
        percent_failure = round(failed_attempts.count() / float(responses.count()) * 100, 2)

    failure_grouping = groupby(sorted(failed_attempts.all(), key=lambda g: g.question_id), key=lambda r: r.question)
    failure_groups = []

    for key, obj in failure_grouping:
        failure_groups.append(list(obj))

    if len(failure_groups) > 0:
        toughest = [c[0].question for c in failure_groups[:3]]

    success_grouping = groupby(sorted(success_attempts.all(), key=lambda i: i.question_id), key=lambda k: k.question)
    success_groups = []

    for key, obj in success_grouping:
        success_groups.append(list(obj))

    # if company.attempts.count() > 0:
    #     completion_rate = round(company.attempts.filter(is_completed=True).count() / float(company.attempts.count()),
    #                             4) * 100
    #
    # date_groupings = groupby(company_responses.all(), key=lambda z: datetime.strptime(z.date_created.
    #                                                                                   strftime('%Y/%m/%d %H'),
    #                                                                                   '%Y/%m/%d %H').time())

    date_groups = []
    for key, obj in date_groups:
        date_groups.append(list(obj))

    # score_card = filter(lambda y: y.score > 0, sorted(company.attempts.all(), key=lambda v: v.score, reverse=True))[:5]
    #
    # total_attempts = groupby(company.attempts.all(), key=lambda b: b.date_created.date())
    # attempts_rate = []

    # for key, item in total_attempts:
    #     attempts_rate.append(list(item))
    #
    # if len(attempts_rate) > 0:
    #     daily_average = sum([len(x) for x in attempts_rate]) / len(attempts_rate)
    #
    # total_responses = groupby(AttemptResponse.objects.all(), key=lambda e: e.date_created.date())
    # responses_rate = []
    #
    # for key, item in total_responses:
    #     responses_rate.append(list(item))
    #
    # if len(responses_rate) > 0:
    #     daily_response = sum([len(x) for x in responses_rate]) / len(responses_rate)

    context = RequestContext(request, processors=[context_processor])

    return render(request, 'app/index2.html', locals(), context)


@decorators.login_required
def view_questions(request, pk):
    """ returns a list of all questions"""
    trivia = get_object_or_404(Trivia, pk=pk)
    questions = Question.objects.filter(trivia=trivia.pk)
    context = RequestContext(request, processors=[context_processor])
    return render(request, 'app/questions.html', locals(), context)


@decorators.login_required
def create_question(request, pk):
    errors = None
    trivia = Trivia.objects.get(pk=pk)

    if request.method == "POST":
        form = forms.QuestionForm(request.POST)

        if form.is_valid():
            data = form.data.copy()
            position = data.get('position')
            problem = data.get('problem')
            incorrect_response = data.get("incorrect_response")
            user = request.user
            question = Question.objects.filter(Q(position=position) | Q(problem=problem), trivia=trivia.pk).first()

            if question is None:

                question = Question.objects.create(trivia=trivia, position=position, problem=problem,
                                                   incorrect_response=incorrect_response)
                response = "/questions/%s/choices" % question.pk
                return HttpResponseRedirect(response)
            else:
                errors = "Question with this position or problem has been created already"

    form = forms.QuestionForm()

    return render(request, 'app/forms/create_question.html', {'form': form, 'errors': errors, 'trivia': trivia})


@decorators.login_required
def update_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    trivia = get_object_or_404(Trivia, pk=question.trivia.pk)

    if request.method == "POST":

        form = forms.QuestionForm(initial=question, data=request.POST)

        if form.is_valid():
            serializer = QuestionSerializer(data=form.data, instance=question)
            if serializer.is_valid():
                serializer.save()
                response = "/trivias/%s/questions" % trivia.pk
                return HttpResponseRedirect(response)

    form = forms.QuestionForm()

    return render(request, 'app/forms/update_question.html', {'form': form, "question": question, 'trivia': trivia})


@decorators.login_required
def delete_question(request, pk):
    question = get_object_or_404(Question, pk=pk)

    trivia = get_object_or_404(Trivia, pk=question.trivia.pk)

    question.delete()

    response = "/trivias/%s/questions" % trivia.pk
    return HttpResponseRedirect(response)


@decorators.login_required
def view_trivias(request):
    """ returns a list of all trivias"""
    context = RequestContext(request, processors=[context_processor])
    return render(request, 'app/trivias.html', locals(), context)


@decorators.login_required
def dashboard(request, pk):
    """ index view
    """
    trivia = get_object_or_404(Trivia, pk=pk)
    players = Player.objects.filter(trivia=trivia.pk)
    responses = AttemptResponse.objects.filter(trivia=trivia.pk)

    latest_users = players.order_by('date_created')[:8]

    failed_attempts = responses.filter(is_correct=False)
    difficulty_rate = groupby(failed_attempts.all(), key=lambda s: s.attempt.current_question)

    success_attempts = responses.filter(is_correct=True)
    success_rate = groupby(success_attempts.all(), key=lambda q: q.attempt.current_question)
    percent_success = percent_failure = completion_rate = daily_average = daily_response = 0
    toughest = None

    if responses.count() > 0:
        percent_success = round(success_attempts.count() / float(responses.count()) * 100, 2)
        percent_failure = round(failed_attempts.count() / float(responses.count()) * 100, 2)

    failure_grouping = groupby(sorted(failed_attempts.all(), key=lambda g: g.question_id), key=lambda r: r.question)
    failure_groups = []

    for key, _obj in failure_grouping:
        failure_groups.append(list(_obj))

    if len(failure_groups) > 0:
        toughest = [c[0].question for c in failure_groups[:3]]

    success_grouping = groupby(sorted(success_attempts.all(), key=lambda i: i.question_id), key=lambda k: k.question)
    success_groups = []

    for key, obj in success_grouping:
        success_groups.append(list(obj))

    if trivia.attempts.count() > 0:
        completion_rate = round(trivia.attempts.filter(is_completed=True).count() / float(trivia.attempts.count()),
                                4) * 100

    date_groupings = groupby(trivia.responses.all(), key=lambda z: datetime.strptime(z.date_created.
                                                                                     strftime('%Y/%m/%d %H'),
                                                                                     '%Y/%m/%d %H').time())

    date_groups = []
    for key, obj in date_groups:
        date_groups.append(list(obj))

    score_card = filter(lambda y: y.score > 0, sorted(trivia.attempts.all(), key=lambda v: v.score, reverse=True))[:5]

    total_attempts = groupby(trivia.attempts.all(), key=lambda b: b.date_created.date())
    attempts_rate = []

    for key, item in total_attempts:
        attempts_rate.append(list(item))

    if len(attempts_rate) > 0:
        daily_average = sum([len(x) for x in attempts_rate]) / len(attempts_rate)

    total_responses = groupby(AttemptResponse.objects.all(), key=lambda e: e.date_created.date())
    responses_rate = []

    for key, item in total_responses:
        responses_rate.append(list(item))

    if len(responses_rate) > 0:
        daily_response = sum([len(x) for x in responses_rate]) / len(responses_rate)

    _update = {
        "attempts": Attempt.objects.filter(trivia=trivia.pk),
        "questions": Question.objects.filter(trivia=trivia.pk),
        "players": Player.objects.filter(trivia=trivia.pk),
        "winners": Draws.objects.filter(trivia=trivia.pk),
        'length': len,
        "limited_questions": Question.objects.filter(trivia=trivia.pk).all()[:5],
        "today": date.today(),
        "responses": AttemptResponse.objects.filter(trivia=trivia.pk),
        "year": date.today().year,
    }

    context = RequestContext(request, dict_=_update)

    return render(request, 'app/dashboard.html', locals(), context)


@decorators.login_required
def create_trivia(request):
    errors = None

    form = forms.TriviaForm()

    if request.method == "POST":
        form = forms.TriviaForm(request.POST)

        if form.is_valid():
            data = form.data.copy()
            name = data.get('name')
            short_code = data.get('short_code')
            trivia = Trivia.objects.filter(name=name, short_code=short_code).first()

            if trivia is None:
                trivia = Trivia.objects.create(name=name, short_code=short_code, invalid=data.get('invalid'),
                                               incorrect_keyword=data.get('incorrect_keyword'),
                                               limit_response=data.get('limit_response'), keyword=data.get('keyword'),
                                               subscribe_message=data.get('subscribe_message'),
                                               service_name=data.get('service_name'),
                                               mt_short_code=data.get('mt_short_code'), draw_time=data.get('draw_time'))
                return HttpResponseRedirect('/trivias')
            else:
                errors = "Trivia with this name and short_code has been created already"

    return render(request, 'app/forms/create_trivia.html', {'form': form, 'errors': errors})


@decorators.login_required
def update_trivia(request, pk):
    trivia = get_object_or_404(Trivia, pk=pk)

    if request.method == "POST":

        data = request.POST.copy()

        draw_time = datetime.strptime(data["draw_time"], "%I:%M %p")
        data["draw_time"] = draw_time.time()

        form = forms.TriviaForm(initial=trivia, data=data)

        if form.is_valid():
            serializer = TriviaSerializer(data=data, instance=trivia)
            if serializer.is_valid():
                serializer.save()
                return HttpResponseRedirect('/trivias')

    form = forms.TriviaForm()

    return render(request, 'app/forms/update_trivia.html', {'form': form, "trivia": trivia})


@decorators.login_required
def delete_trivia(request, pk):
    trivia = get_object_or_404(Trivia, pk=pk)

    trivia.delete()

    return HttpResponseRedirect('/trivias')


@decorators.login_required
def create_choice(request, pk):
    errors = None

    question = get_object_or_404(Question, pk=pk)

    trivia = get_object_or_404(Trivia, pk=question.trivia_id)

    if request.method == "POST":

        data = request.POST.copy()
        data['question_id'] = question.pk

        form = forms.ChoiceForm(data)

        choice = Choices.objects.filter(question=question.pk, code=data['code']).first()

        if choice:
            errors = "Question choice already exists. Please enter both fields again."

        elif form.is_valid():
            serializer = ChoiceSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = '/trivias/%s/questions' % trivia.pk
                return HttpResponseRedirect(response)

    form = forms.QuestionForm()

    return render(request, 'app/forms/create_choice.html', {'form': form, 'question': question, "errors": errors})


@decorators.login_required
def update_choice(request, pk):
    choice = get_object_or_404(Choices, pk=pk)

    if request.method == "POST":

        data = request.POST.copy()
        data["question"] = choice.question_id

        form = forms.ChoiceForm(data, instance=choice)

        if form.is_valid():
            serializer = ChoiceSerializer(data=form.data, instance=choice)
            if serializer.is_valid():
                serializer.save()
                return HttpResponseRedirect('/questions')

    form = forms.ChoiceForm()

    return render(request, 'app/forms/update_choice.html', {'form': form, "choice": choice})


@decorators.login_required()
def delete_choice(request, pk):
    choice = get_object_or_404(Choices, pk=pk)

    trivia = Trivia.objects.get(pk=choice.question.trivia_id)

    choice.delete()

    response = '/trivias/%s/questions' % trivia.pk

    return HttpResponseRedirect(response)


@decorators.login_required
def change_choice(request, pk):
    """ Change correct choice for question """

    choice = get_object_or_404(Choices, pk=pk)

    trivia = Trivia.objects.get(pk=choice.question.trivia.pk)

    choice.is_valid = True
    choice.save()

    question = choice.question

    wrong_choices = filter(lambda x: x.pk != choice.pk, question.choices.all())

    for x in wrong_choices:
        x.is_valid = False
        x.save()

    response = '/trivias/%s/questions' % trivia.pk

    return HttpResponseRedirect(response)


@decorators.login_required
def view_attempts(request, pk):
    """ returns a list of all promos"""
    player = get_object_or_404(Player, pk=pk)
    user_attempts = player.attempts
    context = RequestContext(request, processors=[context_processor])
    return render(request, 'app/attempts.html', locals(), context)


@decorators.login_required
def view_users(request):
    """ returns a list of all promos"""
    context = RequestContext(request, processors=[context_processor])
    return render(request, 'app/users.html', locals(), context)


@decorators.login_required
def view_participants(request, pk):
    """ returns a list of all promos"""
    context = RequestContext(request, processors=[context_processor])

    trivia = Trivia.objects.get(pk=pk)

    if not trivia:
        raise Http404

    group = Attempt.objects.filter(trivia_id=trivia.pk).all()

    items = sorted(group,key=lambda x: x.msisdn)

    participants = []

    for key, group in groupby(items, key=lambda x: x.msisdn):
        participants.append(list(group))

    return render(request, 'app/participants.html', locals(), context)


@decorators.login_required
def view_winners(request, pk):
    """ returns a list of all promos"""
    context = RequestContext(request, processors=[context_processor])

    trivia = Trivia.objects.get(pk=pk)

    if not trivia:
        raise Http404

    winners = Draws.objects.filter(trivia_id=trivia.pk).all()

    return render(request, 'app/winners.html', locals(), context)


@decorators.login_required
def view_reports(request, pk):
    """ returns a list of all promos"""

    trivia = get_object_or_404(Trivia, pk=pk)

    now = datetime.combine(date.today(), time.min)
    later = now + timedelta(days=1)

    questions_today = AttemptResponse.objects.filter(date_created__range=(now, later), trivia_id=trivia.pk)
    participants_today = Attempt.objects.filter(date_modified__range=(now, later), trivia_id=trivia.pk)

    if request.method == "POST":
        data = request.POST.copy()
        start_date = datetime.strptime(data["start_date"], '%d/%m/%Y').date()
        end_date = datetime.strptime(data["end_date"], '%d/%m/%Y').date()

        data["start_date"] = start_date
        data["end_date"] = end_date

        form = forms.ReportForm(data)

        if form.is_valid():
            variable = form.data.get("variable")
            if variable == "winners":
                # data = Draws.objects.filter(date_created__range=(start_date, end_date), promo_id=promo.pk).all()
                data = Attempt.objects.filter(date_created__range=(start_date, end_date), trivia_id=trivia.pk, won=True).all()
                wb = build_draws_sheet(form.data.get('period'), list(data))
                obj = 'winners'
            else:
                if variable == 'response':
                    obj = "AttemptResponse"
                elif variable == 'user':
                    obj = 'Player'
                else:
                    obj = variable.title()

                # start_date = datetime.strptime(form.data["start_date"], '%d/%m/%Y').date()
                # end_date = datetime.strptime(form.data["end_date"], '%d/%m/%Y').date()

                current_module = sys.modules[__name__]
                klass = getattr(current_module, obj)
                data = klass.objects.filter(date_created__range=(start_date, end_date), trivia_id=trivia.pk).all()
                wb = build_spreadsheet(form.data.get('period'), list(data))

            response = HttpResponse(save_virtual_workbook(wb),
                                    content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = "attachment; filename=%s(%s-%s).xlsx" % (obj, start_date, end_date)

            return response

    form = forms.ReportForm()

    context = RequestContext(request, processors=[context_processor])
    return render(request, 'app/reports.html', locals(), context)


def test(request):
    context = RequestContext(request, processors=[context_processor])
    return render(request, "app/loaderio-a8cb7aead3a50b0c2ef4847eacc2634a.txt", locals(), context)


@decorators.login_required
def create_rule(request, pk):
    errors = None

    trivia = get_object_or_404(Trivia, pk=pk)

    if request.method == "POST":

        data = request.POST.copy()
        data['trivia_id'] = trivia.pk

        form = forms.RuleForm(data)

        if form.is_valid():
            serializer = RuleSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = '/trivias/%s/update' % trivia.pk
                return HttpResponseRedirect(response)

    form = forms.RuleForm()

    return render(request, 'app/forms/create_rule.html', {'form': form, 'trivia': trivia, "errors": errors})


@decorators.login_required
def update_rule(request, pk):
    rule = get_object_or_404(Rules, pk=pk)

    if request.method == "POST":

        data = request.POST.copy()
        data["promo_id"] = rule.promo_id

        trivia = get_object_or_404(Trivia, pk=rule.trivia_id)

        form = forms.RuleForm(data, initial=rule)

        if form.is_valid():
            serializer = RuleSerializer(data=data, instance=rule)
            if serializer.is_valid():
                serializer.save()
                response = '/trivias/%s/update' % trivia.pk
                return HttpResponseRedirect(response)

    form = forms.RuleForm()

    return render(request, 'app/forms/update_rule.html', {'form': form, "rule": rule})


@decorators.login_required()
def delete_rule(request, pk):
    rule = get_object_or_404(Rules, pk=pk)

    trivia = Trivia.objects.get(pk=rule.trivia_id)

    rule.delete()

    response = '/trivias/%s/update' % trivia.pk

    return HttpResponseRedirect(response)


@api_view(['GET'])
def daily_attempts_report(request, *args):
    """
    return json data showing daily attempts
    :return:
    """
    now = datetime.combine(date.today(), time(23, 59))
    week_start = datetime.combine(date.today() - timedelta(days=6), time.min)
    data = dict()
    for i, j in enumerate(xrange(7)):
        val = (week_start.date() + timedelta(days=j)).strftime('%Y-%m-%d')
        data.update({val:0})

    responses_data = data.copy()
    attempts_data = data.copy()

    responses_today = AttemptResponse.objects.filter(date_created__range=(week_start, now)).all()
    attempts_today = Attempt.objects.filter(date_modified__range=(week_start, now)).all()

    for key, group in groupby(responses_today, lambda i:i.date_created.date()):
        data_key = key.strftime('%Y-%m-%d')
        responses_data[data_key] = len(list(group))

    for key, group in groupby(attempts_today, lambda i:i.date_created.date()):
        data_key = key.strftime('%Y-%m-%d')
        attempts_data[data_key] = len(list(group))

    return Response({'attempt_keys': attempts_data.keys(), 'attempt_values': attempts_data.values(),
                     'responses_keys': responses_data.keys(), 'responses_values': responses_data.values()},
                    status=status.HTTP_200_OK)