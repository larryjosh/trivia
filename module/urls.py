__author__ = 'stikks'
from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView

from rest_framework.urlpatterns import format_suffix_patterns

import views

urlpatterns = [
   # api urls
   url(r'^$', views.api_root),
   url(r'^trivia$', name="attempts", view=views.AttemptView.as_view()),
   url(r'^new$', name="new", view=views.OptInView.as_view()),
   url(r'^daily-report$', name="new", view=views.daily_attempts_report)
]

urlpatterns = format_suffix_patterns(urlpatterns)