__author__ = 'stikks'

from django import forms
from django.core import validators
from django.utils.text import slugify

from .models import Question, Choices, UserModel, Trivia, Rules


class QuestionForm(forms.Form):
    """ Custom form for creating new promos """
    position = forms.IntegerField(required=True)
    problem = forms.CharField(widget=forms.widgets.TextInput)
    incorrect_response = forms.CharField(widget=forms.widgets.Textarea)

    class Meta:
        fields = ["position", "problem", "incorrect_response"]


class TriviaForm(forms.Form):
    """ Custom form for creating new promos """
    name = forms.CharField(required=True, widget=forms.widgets.TextInput)
    short_code = forms.CharField(required=True, widget=forms.widgets.TextInput)
    invalid = forms.CharField(required=True, widget=forms.widgets.TextInput)
    incorrect_keyword = forms.CharField(required=True, widget=forms.widgets.TextInput)
    limit_response = forms.CharField(required=True, widget=forms.widgets.TextInput)
    keyword = forms.CharField(required=True, widget=forms.widgets.TextInput)
    subscribe_message = forms.CharField(required=True, widget=forms.widgets.TextInput)
    service_name = forms.CharField(required=True, widget=forms.widgets.TextInput)
    mt_short_code = forms.CharField(required=True, widget=forms.widgets.TextInput)
    draw_time = forms.TimeField(required=True)
    draw_winners = forms.IntegerField()

    class Meta:
        fields = ["name", "short_code", "invalid", "incorrect_keyword", "limit_response", "keyword",
                  "subscribe_message", "service_name", "mt_short_code", "draw_time", "draw_winners"]


class ChoiceForm(forms.Form):
    """ model form for creating question choices """
    code = forms.CharField(required=True, widget=forms.widgets.TextInput)
    value = forms.CharField(widget=forms.widgets.TextInput, required=True)

    class Meta:
        model = Choices
        fields = ["question", "code", "value"]


class RegistrationForm(forms.ModelForm):
    """
    Form for registering a new account.
    """
    email = forms.EmailField(widget=forms.widgets.TextInput, label="Email")
    password = forms.CharField(widget=forms.widgets.PasswordInput,
                               label="Password")
    verify_password = forms.CharField(widget=forms.widgets.PasswordInput,
                                      label="Password (again)")

    class Meta:
        model = UserModel
        fields = ['email', 'password', 'verify_password']

    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        cleaned_data = super(RegistrationForm, self).clean()
        if 'password' in cleaned_data and 'verify_password' in cleaned_data:
            if self.cleaned_data['password'] != self.cleaned_data['verify_password']:
                raise forms.ValidationError({"password": "Passwords don't match. Please enter both fields again."})

        return self.cleaned_data

    def save(self, commit=True, company_id=None):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class AuthenticationForm(forms.Form):
    email = forms.EmailField(widget=forms.widgets.TextInput)
    password = forms.CharField(widget=forms.widgets.PasswordInput)

    class Meta:
        fields = ['email', 'password']


# class CompanyRegistrationForm(forms.ModelForm):
#     """
#     Form for registering a new account.
#     """
#     name = forms.CharField(widget=forms.widgets.TextInput, required=True)
#     email = forms.EmailField(widget=forms.widgets.TextInput, required=True, validators=[validators.validate_email])
#     password = forms.CharField(widget=forms.widgets.PasswordInput, required=True)
#     verify_password = forms.CharField(widget=forms.widgets.PasswordInput, required=True)
#
#     class Meta:
#         model = Company
#         fields = ['name','email']
#
#     def clean(self):
#         """
#         Verifies that the values entered into the password fields match
#
#         NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
#         """
#         cleaned_data = super(CompanyRegistrationForm, self).clean()
#         if cleaned_data['password'] != cleaned_data['verify_password']:
#             raise forms.ValidationError({"password": "Passwords don't match. Please enter both fields again."})
#         return cleaned_data
#
#     def save(self, commit=True):
#         data = super(CompanyRegistrationForm, self).clean()
#         company = Company.objects.create(name=data['name'], slug=slugify(data["name"]))
#         user = UserModel.objects.create(email=data['email'], password=data['password'], company=company)
#         user.set_password(data['password'])
#         return user


class ReportForm(forms.Form):
    period = forms.CharField(required=True)
    variable = forms.CharField(required=True)
    start_date = forms.DateField(required=True)
    end_date = forms.CharField(required=True)


class RuleForm(forms.Form):
    """ model form for creating question choices """
    number = forms.CharField(required=True, widget=forms.widgets.TextInput)
    response = forms.CharField(widget=forms.widgets.TextInput, required=True)

    class Meta:
        model = Rules
        fields = ["trivia", "number", "response"]