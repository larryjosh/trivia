"""
Django settings for fanta project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
STATIC_ROOT = os.path.join(BASE_DIR, 'module/static')

import djcelery

djcelery.setup_loader()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'zbw(u=2%&x88t$yp!7_#2h3g4czkipf=xgxveigora1@o^xs6y'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ["*"]

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_swagger',
    'module',
    'mathfilters',
    'django_gearman',
    'djcelery'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'fanta.urls'

WSGI_APPLICATION = 'fanta.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

# STATIC_URL = os.path.join(BASE_DIR, 'module/static/')
STATIC_URL = '/static/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'trivia',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'trivia',
        'PASSWORD': '',
        'HOST': 'localhost',            # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

INCORRECT_KEYWORD_MESSAGE = "We do not recognize this keyword."

# LIMIT_RESPONSE = "Great! You have answered the maximum questions allowed per week and entered into the draw to win a PS4" \
#                  " this week. Play again next week, text FANTA to 5030"
#
# INVALID_CODE = "You have answered the maximum questions allowed per week text your name and phone number to 5021 to " \
#                "claim your prize. Play again next week, text Fanta to 5030"


PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
]

AUTH_USER_MODEL = 'module.UserModel'
AUTHENTICATION_BACKENDS = ['module.backends.EmailAuthBackend', ]

GEARMAN_SERVERS = ['127.0.0.1']
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Africa/Lagos'


SERVICE_IDS = {'etisalat_bc': 4510, 'etisalat_content': 4510}
SEVAS_USERNAME = "sponge"
SEVAS_PASSWORD = "sponge"
SEND_SMS_API = 'http://sponge.atp-sevas.com:13031/cgi-bin/sendsms'


SUBSCRIPTION_SERVICE_NAME = "TRIVIA"
GAME_SERVICE_NAME = "GAME"
SUBSCRIPTION_SERVICE_SHORT_CODE = 32682
SUBSCRIBE_MESSAGE = "Text %s to subscribe to this service" % SUBSCRIPTION_SERVICE_NAME

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"


MONGOD_HOST = 'localhost'
MONGOD_PORT = 27017
MONGOD_DATABASE = 'kaadie'
MONGO_CONNECT = False
