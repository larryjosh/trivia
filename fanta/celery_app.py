from __future__ import absolute_import

import os

from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fanta.settings')

from django.conf import settings

celery_app = Celery('trivia')

celery_app.config_from_object('django.conf:settings')
celery_app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

celery_app.conf.update(
    CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
)