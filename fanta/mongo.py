from pymongo import MongoClient
from django.conf import settings

mongo_client = MongoClient(settings.MONGOD_HOST, settings.MONGOD_PORT, connect=False)