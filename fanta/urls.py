from django.conf.urls import patterns, include, url

from module import views

# handler404 = views.not_found
# handler500 = views.server_error

urlpatterns = [

    url(r'^api_auth/', include('rest_framework.urls'), name='rest-framework'),
    url(r'^api/v1/', include('module.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^$', views.index),
    url(r'^trivias/(?P<pk>[0-9]+)/questions$', views.view_questions, name="view_questions"),
    url(r'^trivias/(?P<pk>[0-9]+)/dashboard$', views.dashboard, name="dashboard"),
    url(r'^trivias/(?P<pk>[0-9]+)/questions/create$', views.create_question),
    url(r'^questions/(?P<pk>[0-9]+)/update$', views.update_question),
    url(r'^questions/(?P<pk>[0-9]+)/delete$', views.delete_question, name="delete-question"),
    url(r'^trivias$', views.view_trivias),
    url(r'^trivias/create$', views.create_trivia),
    url(r'^trivias/(?P<pk>[0-9]+)/update$', views.update_trivia, name="update-trivia"),
    url(r'^trivias/(?P<pk>[0-9]+)/delete$', views.delete_trivia, name="delete-trivia"),
    url(r'^questions/(?P<pk>[0-9]+)/choices$', views.create_choice, name="create-choice"),
    url(r'^choices/(?P<pk>[0-9]+)/update$', views.update_choice),
    url(r'^choices/(?P<pk>[0-9]+)/delete$', views.delete_choice, name="delete_choice"),
    url(r'^choices/(?P<pk>[0-9]+)/change$', views.change_choice, name="change-choice"),
    url(r'^players', views.view_users),
    url(r'^players/(?P<pk>[0-9]+)/attempts$', views.view_attempts),
    url(r'^trivias/(?P<pk>[0-9]+)/winners', views.view_winners, name='view_winners'),
    url(r'^trivias/(?P<pk>[0-9]+)/reports', views.view_reports, name='view_reports'),
    url(r'^trivias/(?P<pk>[0-9]+)/rules', views.create_rule, name='rules'),
    url(r'^trivias/(?P<pk>[0-9]+)/participants$', views.view_participants, name='view_participants'),
    url(r'^rules/(?P<pk>[0-9]+)/update$', views.update_rule),
    url(r'^rules/(?P<pk>[0-9]+)/delete$', views.delete_rule, name="delete_rule"),
    # url(r'^accounts/register$', views.register, name='register'),
    # url(r'^company/register$', views.register_company, name='register-company'),
    url(r'^accounts/login/$', views.login, name='login'),
    url(r'^accounts/logout$', views.logout, name='logout'),
    # url(r'^winners$', views.view_winners),
    # url(r'^reports', views.view_reports),
    url(r'^loaderio-a8cb7aead3a50b0c2ef4847eacc2634a/', views.test)
]
